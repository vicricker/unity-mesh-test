﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshTest : MonoBehaviour {
	const int size = 130; // Size of mesh size x size
	const int size2 = size/2;
	const float scale = 0.1f; // Scale of vertices

	Vector3[] newVertices;
	Vector2[] newUV;
	int[] newTriangles;

	void Start() {
		// Replace the cube with a new mesh to be deformed
		Mesh mesh = GetComponent<MeshFilter>().mesh;

		newVertices = new Vector3[size * size];
		newTriangles = new int[size *size*6];
		newUV = new Vector2[size * size];

		float center = (float)(size2 * scale);
		int n = 0;
		for (int y=0 ; y < size; y++) {
			for (int x = 0; x < size; x++) {
				newVertices [n] = new Vector3 ((float)(x*scale-center), (float)(y*scale-center), 0.0f); // Generate vertices relative to the center (such that 0,0 is the center of the mesh)

				newUV [n++] = new Vector2 ((float)(.75*x/size), (float)(.75*y/size)); // I'm not 100% sure of how to specify the UV coords, but this seems to work acceptably
			}
		}

		// Fill the mesh with triangles
		n = 0;
		for (int y=0 ; y < size-1; y++) {
			for (int x = 0; x < size-1; x++) {
				int corner = y * size + x;

				newTriangles [n++] = corner;
				newTriangles [n++] = corner + size + 1;
				newTriangles [n++] = corner + size;

				newTriangles [n++] = corner;
				newTriangles [n++] = corner + 1;
				newTriangles [n++] = corner + size+1;
			}
		}

		mesh.Clear();

		mesh.vertices = newVertices;
		mesh.uv = newUV;
		mesh.triangles = newTriangles;

	 	mesh.RecalculateBounds();
 		mesh.RecalculateNormals();
	}

	float offset = 0;

	// Recalc the shape
	void Update () {
		transform.Rotate(Vector3.back * Time.deltaTime * 20);  // Rotate object

  		Mesh mesh = GetComponent<MeshFilter>().mesh;

		Vector3[] verts = mesh.vertices;

		Vector3 mid = new Vector3(size / 2.0f, size/2.0f, 0.0f); // midpoint of the mesh from which distance calculations are performed

		int index = 0;
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				double distance=Vector2.Distance(mid, new Vector3(x, y, 0.0f)); // Distance from the center
				double distancePct=distance/(size2); // Note that since the distance can be larger than size2, the mesh turns up at the corners (I thought it looked cool)
				float magnitude = (float)(1-distancePct) * 3; // magnitude decreases with distance from the center

				verts [index++].z = magnitude*(float)(Mathf.Sin ((float)distance/5-offset)); // Calculate the height of the vertex
			}
		}

		mesh.vertices = verts; // Have to reassign the vertex array or it won't take effect
		mesh.RecalculateNormals(); // Recalculate the normals so the lighting, etc. looks acurate

		offset += Time.deltaTime*2; // Change the offset to animate the 'wave'
	}
}
